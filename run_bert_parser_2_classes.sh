#!/usr/bin/env bash

path=$1;
out=$2;

for dataset in  aisopos_ntua debate english_dailabor nikolaos_ted pang_movie \
    sanders sarcasm sentistrength_bbc sentistrength_digg sentistrength_myspace \
    sentistrength_rw sentistrength_twitter sentistrength_youtube stanford_tweets \
    tweet_semevaltest vader_amazon vader_movie vader_nyt vader_twitter yelp_reviews ; do
    mkdir "${out}/${dataset}"
    for fold in 0 1 2 3 4; do
        python3 parse_input_bert_sentiment_2_classes.py -t "${path}/${dataset}/d_train_data_${fold}.txt" \
        -l "${path}/${dataset}/c_train_data_${fold}.txt" -n 2 -o "${out}/${dataset}/train${fold}.csv" -c train;

        python3 parse_input_bert_sentiment_2_classes.py -t "${path}/${dataset}/d_test_data_${fold}.txt" \
        -l "${path}/${dataset}/c_test_data_${fold}.txt" -n 2 -o "${out}/${dataset}/d_test${fold}" \
        -f "${out}/${dataset}/c_test${fold}" -c test;
    done
done
