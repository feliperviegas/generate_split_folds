import numpy as np
import re
import argparse


def indices_to_one_hot(data, nb_classes):
    """Convert an iterable of indices to one-hot encoded labels."""
    targets = np.array(data).reshape(-1)
    return np.eye(nb_classes)[targets]


def read_labels(input_file):
    y = []
    docs_id = []
    doc_id = 1
    with open('{}'.format(input_file), 'r') as labels:
        for label in labels:
            y.append(label.strip())
            docs_id.append(doc_id)
            doc_id += 1

        labels.close()

    return np.array(y, dtype=np.int32), docs_id


def read_texts(input_file, docs_id):
    x = []
    doc_id = 1
    with open('{}'.format(input_file), 'r') as texts:
        for text in texts:
            if doc_id in docs_id:
                text_re = re.sub(r',', ' ', text)
                x.append(text_re.strip())
            
            doc_id += 1

    return np.array(x)


def save_bert_train_file(x, y, n_classes, output_file):
    with open('{}'.format(output_file), 'w') as out:
        for _class in range(0, n_classes):
            out.write('class_{},'.format(_class))

        out.write('text\n')
        for text in range(0, y.shape[0]):
            for it_onehot in range(0, n_classes):
                out.write('{},'.format(int(y[text][it_onehot])))

            out.write('{}\n'.format(x[text]))

        out.close()


def save_bert_test_file(x, y, output_file, class_output):
    with open('{}'.format(output_file), 'w') as out:
        for text in range(0, y.shape[0]):
            out.write('{}\n'.format(x[text]))

        out.close()

    with open('{}'.format(class_output), 'w') as out:
        for doc in y:
            out.write(f'{doc}\n')

        out.close()


def run():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--texts', 
                        action='store', 
                        type=str,
                        dest='texts',
                        help='-t [texts file name]')
    parser.add_argument('-l', '--labels', 
                        action='store', 
                        type=str,
                        dest='labels',
                        help='--labels [labels file name]')
    parser.add_argument('-n', '--n_classes', 
                        action='store', 
                        type=int,
                        dest='n_classes',
                        help='--n_classes [number of classes]')
    parser.add_argument('-o', '--output', 
                        action='store', 
                        type=str,
                        dest='output',
                        help='--output [output file]')
    parser.add_argument('-c', '--choice', 
                        action='store', 
                        type=str,
                        dest='type',
                        help='--choice [train or test]')
    parser.add_argument('-f', '--output_class', 
                        action='store', 
                        type=str,
                        dest='output_class',
                        help='--output_class [output class file]')
    args = parser.parse_args()

    # print(args.texts)
    # print(args.labels)
    # print(args.n_classes)

    y, docs_id = read_labels(args.labels)
    y_one_hot = indices_to_one_hot(y, args.n_classes)
    x = read_texts(args.texts, docs_id)
    if args.type == 'train':
        save_bert_train_file(x=x, y=y_one_hot, n_classes=args.n_classes, output_file=args.output)
    elif args.type == 'test':
        save_bert_test_file(x=x, y=y, output_file=args.output, class_output=args.output_class)


if __name__ == '__main__':
    run()
