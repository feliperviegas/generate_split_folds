#!/bin/bash


for dataset in  aisopos_ntua debate english_dailabor nikolaos_ted pang_movie \
    sanders sarcasm sentistrength_bbc sentistrength_digg sentistrength_myspace \
    sentistrength_rw sentistrength_twitter sentistrength_youtube stanford_tweets \
    tweet_semevaltest vader_amazon vader_movie vader_nyt vader_twitter yelp_reviews ; do
  echo "${dataset} ";
  # shellcheck disable=SC2006
  for fold in `seq 0 1 4`; do
    echo "Fold - ${fold}";

    python3 parse_splits.py -t sentiment_2_classes/${dataset}/texts.txt \
    -l sentiment_2_classes/${dataset}/scores.txt \
    -s sentiment_2_classes/${dataset}/splits_${dataset}_5.csv \
    -f "${fold}" -o sentiment_2_classes/${dataset}

    echo -n 'Training features DIFF='
    diff sentiment_2_classes/${dataset}/d_train_data_"${fold}".txt \
    sentiment_dataset_2_classes/${dataset}/texts/treino"${fold}"_temp | wc -l

    rm sentiment_2_classes/${dataset}/d_train_data_"${fold}".txt;

    echo -n 'Test features DIFF='
    diff sentiment_2_classes/${dataset}/d_test_data_"${fold}".txt \
    sentiment_dataset_2_classes/${dataset}/texts/teste"${fold}"_temp | wc -l

    rm sentiment_2_classes/${dataset}/d_test_data_"${fold}".txt

    echo -n 'Training labels DIFF='
    diff sentiment_2_classes/${dataset}/c_train_data_"${fold}".txt \
    sentiment_dataset_2_classes/${dataset}/classes/treino"${fold}"_temp | wc -l

    rm sentiment_2_classes/${dataset}/c_train_data_"${fold}".txt

    echo -n 'Test labels DIFF='
    diff sentiment_2_classes/${dataset}/c_test_data_"${fold}".txt \
    sentiment_dataset_2_classes/${dataset}/classes/teste"${fold}"_temp | wc -l

    rm sentiment_2_classes/${dataset}/c_test_data_"${fold}".txt

  done
done
