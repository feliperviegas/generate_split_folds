#!/bin/bash

path=$1;
out=$2;

for dataset in  aisopos_ntua debate english_dailabor nikolaos_ted pang_movie \
    sanders sarcasm sentistrength_bbc sentistrength_digg sentistrength_myspace \
    sentistrength_rw sentistrength_twitter sentistrength_youtube stanford_tweets \
    tweet_semevaltest vader_amazon vader_movie vader_nyt vader_twitter yelp_reviews ; do

  echo "${dataset} ";

  mkdir "${out}/${dataset}";

  for fold in $(seq 0 1 4); do
    echo "Fold - ${fold}";

    python3 parse_splits.py -t "${path}/${dataset}/texts.txt" \
    -l "${path}/${dataset}/score_pre.txt" \
    -s "${path}/${dataset}/representations/split_5.csv" \
    -f "${fold}" -o "${out}/${dataset}"

  done
done
