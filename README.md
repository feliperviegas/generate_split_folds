# Generate Split Folds

Build docker container:

```docker build -t generate_split_folds <project_path>```

Run docker container:

```docker run --rm --name generate_split_folds -v <project_path>/:/generate_split_folds -i -t generate_split_folds /bin/bash```

For more information about building and running a docker container, see: https://docs.docker.com/

Then, run the code:

```export LANG="C.UTF-8"```

To run:

```bash gen_split_folds.sh```


Check correctness:

```bash check_splits.sh```