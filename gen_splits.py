import os
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset',
                        action='store',
                        type=str,
                        dest='dataset',
                        required=True,
                        help='-d [dataset folder name]')
    parser.add_argument('-t', '--texts',
                        action='store',
                        type=str,
                        dest='texts',
                        default='',
                        help='-t [texts folder name]')
    parser.add_argument('-f', '--folds',
                        action='store',
                        type=int,
                        required=True,
                        dest='folds',
                        default='',
                        help='-f [number of folds]')
    parser.add_argument('-dt', '--dtrain',
                        action='store',
                        type=str,
                        dest='dtrain',
                        default='',
                        help='-dt [root path of files treino<fold>_temp]')
    parser.add_argument('-dte', '--dtest',
                        action='store',
                        type=str,
                        dest='dtest',
                        default='',
                        help='-dte [root path of files teste<fold>_temp]')
    parser.add_argument('-o', '--output',
                        action='store',
                        type=str,
                        dest='output',
                        default='',
                        help='-o [output path]')
    args = parser.parse_args()

    docs_id, docs = read_document(args.texts)

    splits_dict = dict()
    with open(f'{args.output}/splits_{args.dataset}_{args.folds}.csv', 'w') as split_output:
        for fold in range(0, args.folds):
            split_fold = read_train_test_fold(docs_id=docs_id,
                                              docs=docs,
                                              input_train=args.dtrain,
                                              input_test=args.dtest,
                                              fold=fold)
            splits_dict[fold] = split_fold
            split_output.write(f'{split_fold}\n')

        split_output.close()


def read_document(input_file):
    docs_id = list()
    docs = list()
    with open(input_file, encoding='utf8', errors='ignore') as documents:
        for document in documents:
            split_doc = document.strip().split(' ')
            docs_id.append(int(split_doc[0]))
            docs.append(' '.join(split_doc[index] for index in range(1, len(split_doc))).strip())

        documents.close()

    return docs_id, docs


def read_train_test_fold(docs_id, docs, input_train, input_test, fold):
    train_ids = list()
    with open(f'{input_train}/treino{fold}_temp', encoding='utf8', errors='ignore') as training_documents:
        for document in training_documents:
            train_doc = document.strip()
            try:
                index = docs.index(train_doc)
            except ValueError:
                print(train_doc)
                exit(1)

            train_ids.append(docs_id[index])

        training_documents.close()

    test_ids = list()
    with open(f'{input_test}/teste{fold}_temp', encoding='utf8', errors='ignore') as test_documents:
        for document in test_documents:
            test_doc = document.strip()
            try:
                index = docs.index(test_doc)
            except ValueError:
                print(test_doc)
                exit(1)

            test_ids.append(docs_id[index])

        test_documents.close()

    return ' '.join(str(id-1) for id in train_ids) + ";" + ' '.join(str(id-1) for id in test_ids)


if __name__ == '__main__':
    main()
