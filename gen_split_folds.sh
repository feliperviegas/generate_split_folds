#!/bin/bash

mkdir sentiment_2_classes

for dataset in  aisopos_ntua debate english_dailabor nikolaos_ted pang_movie \
    sanders sarcasm sentistrength_bbc sentistrength_digg sentistrength_myspace \
    sentistrength_rw sentistrength_twitter sentistrength_youtube stanford_tweets \
    tweet_semevaltest vader_amazon vader_movie vader_nyt vader_twitter yelp_reviews ; do

  echo "${dataset} ";

  mkdir sentiment_2_classes/${dataset}
  python3 gen_splits.py -d ${dataset} \
    -t sentiment_dataset_2_classes/${dataset}/texts/${dataset}_texts.txt \
    -dt sentiment_dataset_2_classes/${dataset}/texts -dte sentiment_dataset_2_classes/${dataset}/texts -f 5 \
    -o sentiment_2_classes/${dataset};

  cp sentiment_dataset_2_classes/${dataset}/classes/${dataset}_texts.txt sentiment_2_classes/${dataset}/scores.txt

  cut -f 1 -d ' ' --complement sentiment_dataset_2_classes/${dataset}/texts/${dataset}_texts.txt \
  > sentiment_2_classes/${dataset}/texts.txt

done
